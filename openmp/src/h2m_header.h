#ifndef __H2M_HEADER_H__
#define __H2M_HEADER_H__

#include <cstdlib>
#include <new>
#include <limits>
#include <iostream>
#include <vector>

#ifndef RECORD_TRANSITIONS
#define RECORD_TRANSITIONS 0
#endif

#ifndef USE_H2M_ALLOC_ABSTRACTION
#define USE_H2M_ALLOC_ABSTRACTION 0
#endif

#ifndef FINE_GRANULAR_TRANSITIONS
#define FINE_GRANULAR_TRANSITIONS 0
#endif

#if USE_H2M_ALLOC_ABSTRACTION || RECORD_TRANSITIONS
#include "h2m.h"
#endif

#include <cstdlib>
#include <new>
#include <limits>
#include <iostream>
#include <vector>

// ========================================
// === Determine IDs for data items
// ========================================
static int ID_GET_ID_GENERIC(const char* name) {
    char *tmp = nullptr;
    tmp = std::getenv(name);
    if(tmp) {
        return std::atoi(tmp);
    }
    return -1;
}

static int ID_MAT_ROWS()            { return ID_GET_ID_GENERIC("ID_MAT_ROWS"); }            // => 5
static int ID_MAT_ROW_OFFSET()      { return ID_GET_ID_GENERIC("ID_MAT_ROW_OFFSET"); }      // => 12
static int ID_MAT_PACKED_COL()      { return ID_GET_ID_GENERIC("ID_MAT_PACKED_COL"); }      // => 9
static int ID_MAT_PACKED_COEFF()    { return ID_GET_ID_GENERIC("ID_MAT_PACKED_COEFF"); }    // => 10
static int ID_VEC_B()               { return ID_GET_ID_GENERIC("ID_VEC_B"); }               // => 13
static int ID_VEC_X()               { return ID_GET_ID_GENERIC("ID_VEC_X"); }               // => 14

static int ID_VEC_R()               { return ID_GET_ID_GENERIC("ID_VEC_R"); }               // => 2
static int ID_VEC_P()               { return ID_GET_ID_GENERIC("ID_VEC_P"); }               // => 3
static int ID_VEC_AP()              { return ID_GET_ID_GENERIC("ID_VEC_AP"); }              // => 4

// ========================================
// === Custom H2M Allocator for STL
// ========================================
template<class T>
struct H2M_Allocator
{
    typedef T value_type;
 
    H2M_Allocator () = default;
    H2M_Allocator (int id) : __id(id) { };
 
    template<class U>
    constexpr H2M_Allocator (const H2M_Allocator <U>&) noexcept {}
 
    T* allocate(std::size_t n)
    {
        int err;
        if (n > std::numeric_limits<std::size_t>::max() / sizeof(T)) {
            throw std::bad_array_new_length();
        }

#if USE_H2M_ALLOC_ABSTRACTION
        if (auto p = static_cast<T*>(h2m_alloc_w_traits_file(n * sizeof(T), __id, &err))) {
            report(p, n);
            return p;
        }
#else
        if (auto p = static_cast<T*>(std::malloc(n * sizeof(T)))) {
            report(p, n);
            return p;
        }
#endif // USE_H2M_ALLOC_ABSTRACTION

        throw std::bad_alloc();
    }
 
    void deallocate(T* p, std::size_t n) noexcept
    {
        report(p, n, 0);
#if USE_H2M_ALLOC_ABSTRACTION
        h2m_free(p);
#else
        std::free(p);
#endif // USE_H2M_ALLOC_ABSTRACTION
    }
private:
    int __id = -1;
    void report(T* p, std::size_t n, bool alloc = true) const
    {
        std::cerr << "H2M_Allocator (ID=" << __id << ") - " <<(alloc ? "Alloc: " : "Dealloc: ")  << sizeof(T) * n
                  << " bytes at " << std::hex << std::showbase
                  << reinterpret_cast<void*>(p) << std::dec << '\n';
    }
};

#endif // __H2M_HEADER_H__
